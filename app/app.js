/*global angular */

'use strict';

// Declare app level module which depends on views, and components
angular.module('flaghunter', [
    'ngRoute',
    'flaghunter.flagratio',
    'flaghunter.flaghunter',
    'flaghunter.view2',
    'flaghunter.about'
]).
    config(['$routeProvider', '$httpProvider', function ($routeProvider) {
        $routeProvider.otherwise({redirectTo: '/flaghunter'});
    }]);
