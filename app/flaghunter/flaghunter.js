/*global angular */

'use strict';

angular.module('flaghunter.flaghunter', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/flaghunter', {
            templateUrl: 'flaghunter/flaghunter.html',
            controller: 'flaghunterCtrl'
        });
    }])

    .controller('flaghunterCtrl', function ($scope, $http) {

        // get last update time
        $http.get("https://flaghunters.gitlab.io/flaghunter-backend/update_timestamp.txt?timestamp=" + Date.now())
            .then(function (response) {
                $scope.flag_update = new Date(response.data*1000).toLocaleString();
            },function (error){
                // nothing lol
            });

        // get attendance
        $http.get("https://flaghunters.gitlab.io/flaghunter-backend/attendance.json?timestamp=" + Date.now())
            .then(function (response) {
                $scope.hits = response.data.runs;

                //get flags
                $http.get("https://flaghunters.gitlab.io/flaghunter-backend/latest.json?timestamp=" + Date.now())
                    .then(function (response) {
                        $scope.countries = response.data;
                        for (var i = 0; i < $scope.countries.length; i++) {
                            //calc attendance
                            if ($scope.countries[i].attendance === 0) {
                                $scope.countries[i].presence = 0;
                            } else {
                                $scope.countries[i].presence = (parseInt($scope.countries[i].attendance) / parseInt($scope.hits)).toFixed(7) * 100;
                            }
                        }


                        $scope.sortType = 'name';
                        $scope.sortReverse = false;
                        $scope.search = '';

                        $scope.getRows = function () {
                            if (typeof $scope.countries === 'undefined') {
                                return;
                            }
                            //setup filtering

                            //start filtering
                            if ($scope.showAllFlags) {
                                return $scope.countries;
                            } else {
                                var filteredCountriesList = [];

                                for (var i = 0; i < $scope.countries.length; i++) {
                                    if ($scope.countries[i].post_nr_int !== undefined ||
                                        $scope.countries[i].post_nr_pol !== undefined ||
                                        $scope.countries[i].post_nr_sp !== undefined ||
                                        $scope.countries[i].post_nr_bant !== undefined) {
                                        filteredCountriesList.push($scope.countries[i]);
                                    }
                                }

                                return filteredCountriesList;
                            }
                        };

                        //column hiding
                        $scope.columnInt = true;
                        $scope.columnPol = true;
                        $scope.columnBant = true;
                        $scope.columnSp = true;

                        $scope.columnPresence = true;
                    },function (error){
                      // nothing lol
                    });
            },function (error){
                // nothing lol
            });
    });

