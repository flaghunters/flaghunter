/*global angular */

'use strict';

angular.module('flaghunter.flagratio', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/flagratio', {
            templateUrl: 'flaghunter/flagratio.html',
            controller: 'flagratioCtrl'
        });
    }])

    .controller('flagratioCtrl', function ($scope, $http) {

        $http.get("http://whatisthisimnotgoodwithcomputers.com/int/php/get_stats.php")
            .success(function (response) {
                $scope.tokens = response;
                for (var i = 0; i < $scope.tokens.length; i++) {
                    if ($scope.tokens[i].token === 'flag_update') {
                        $scope.flag_update = $scope.tokens[i].value;
                    } else if ($scope.tokens[i].token === 'pop_update') {
                        $scope.pop_update = $scope.tokens[i].value;
                    }
                }
            });

        //get flags
        $http.get("http://whatisthisimnotgoodwithcomputers.com/int/php/get_flagratio.php")
            .success(function (response) {
                $scope.countries = response;
                for (var i = 0; i < $scope.countries.length; i++) {
                    $scope.countries[i].posts_total = parseInt($scope.countries[i].posts_pol) + (parseInt($scope.countries[i].posts_int) + parseInt($scope.countries[i].posts_int_2))
                        + parseInt($scope.countries[i].posts_sp);
                    $scope.countries[i].ratio_int = ((parseInt($scope.countries[i].posts_int) + parseInt($scope.countries[i].posts_int_2)) / parseInt($scope.countries[i].population)).toFixed(7);
                    $scope.countries[i].ratio_pol = (parseInt($scope.countries[i].posts_pol) / parseInt($scope.countries[i].population)).toFixed(7);
                    $scope.countries[i].ratio_sp = (parseInt($scope.countries[i].posts_sp) / parseInt($scope.countries[i].population)).toFixed(7);
                    $scope.countries[i].ratio_total = ((parseInt($scope.countries[i].posts_pol) + (parseInt($scope.countries[i].posts_int) + parseInt($scope.countries[i].posts_int_2))
                    + parseInt($scope.countries[i].posts_sp)) / parseInt($scope.countries[i].population)).toFixed(7);

                    //properly format numbers
                    $scope.countries[i].posts_int = (parseInt($scope.countries[i].posts_int) + parseInt($scope.countries[i].posts_int_2));
                    $scope.countries[i].posts_pol = parseInt($scope.countries[i].posts_pol);
                    $scope.countries[i].posts_sp = parseInt($scope.countries[i].posts_sp);
                    $scope.countries[i].population = parseInt($scope.countries[i].population);

                    //remove nan
                    $scope.countries[i].ratio_int = checkIfNaN($scope.countries[i].ratio_int);
                    $scope.countries[i].ratio_pol = checkIfNaN($scope.countries[i].ratio_pol);
                    $scope.countries[i].ratio_sp = checkIfNaN($scope.countries[i].ratio_sp);
                    $scope.countries[i].ratio_total = checkIfNaN($scope.countries[i].ratio_total);

                }
            });

        $scope.sortType = 'name';
        $scope.sortReverse = false;
        $scope.search = '';

        /**
         * Modifiers
         * M micro
         * I mini
         * X Non
         * A Asia
         * F Africa
         * E Europe
         * O Oceania
         * N North America
         * S South America
         * Q Others
         */
        $scope.showMicroCountries = false;
        $scope.showMiniCountries = true;
        $scope.showNonCountries = false;
        $scope.showAsia = true;
        $scope.showAfrica = true;
        $scope.showEurope = true;
        $scope.showOceania = true;
        $scope.showNorthAmerica = true;
        $scope.showSouthAmerica = true;
        $scope.showOthers = true;

        $scope.getRows = function () {
            if (typeof $scope.countries === 'undefined') {
                return;
            }
            //setup filtering
            var filteredCountriesList = [];
            var filtersApplied = [];

            //add filters
            if ($scope.showMicroCountries === false) {
                filtersApplied.push('M');
            }
            if ($scope.showMiniCountries === false) {
                filtersApplied.push('I');
            }
            if ($scope.showNonCountries === false) {
                filtersApplied.push('X');
            }
            if ($scope.showAsia === false) {
                filtersApplied.push('A');
            }
            if ($scope.showAfrica === false) {
                filtersApplied.push('F');
            }
            if ($scope.showEurope === false) {
                filtersApplied.push('E');
            }
            if ($scope.showOceania === false) {
                filtersApplied.push('O');
            }
            if ($scope.showNorthAmerica === false) {
                filtersApplied.push('N');
            }
            if ($scope.showSouthAmerica === false) {
                filtersApplied.push('S');
            }
            if ($scope.showOthers === false) {
                filtersApplied.push('Q');
            }

            //start filtering
            for (var i = 0; i < $scope.countries.length; i++) {
                var countryMayPass = true;
                for (var j = 0; j < filtersApplied.length; j++) {
                    if ($scope.countries[i].modifiers.indexOf(filtersApplied[j]) > -1) {
                        countryMayPass = false;
                    }
                }
                if (countryMayPass === true) {
                    filteredCountriesList.push($scope.countries[i]);
                }
            }

            return filteredCountriesList;
        };

        //column hiding
        $scope.columnIntPosts = true;
        $scope.columnPolPosts = false;
        $scope.columnSpPosts = false;
        $scope.columnTotalPosts = true;

        $scope.columnPopulation = true;

        $scope.columnIntPpp = false;
        $scope.columnPolPpp = false;
        $scope.columnSpPpp = false;
        $scope.columnTotalPpp = true;

    });

function checkIfNaN(val) {
    if (isNaN(val))
        return 'Should be impossible :^)';
    else
        return val;
}

