/*global angular */

'use strict';

angular.module('flaghunter.view2', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/view2', {
            templateUrl: 'view2/view2.html',
            controller: 'View2Ctrl'
        });
    }])

    .controller('View2Ctrl', [function () {
        window.location.replace("https://flaghunters.github.io/Extra-Flags-for-4chan/");
    }]);